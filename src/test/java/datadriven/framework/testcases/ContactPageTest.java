package datadriven.framework.testcases;

import datadriven.framework.pageobject.ContactUs;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class ContactPageTest {

    WebDriver driver;
    ContactUs con;

    @BeforeClass
    public void clickcontactpage()
    {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php?id_category=8&controller=category");
    }


    @Test
    public void form()
    {
        con = new ContactUs(driver);
        WebElement element =con.keyContactClick()
                .keyClickSubjectHeading("2")
                .keyEmail("hello@gmail.com")
                .keyOrderReference("hugduygyg")
                .keyMessage("hello world")
                .keySend();
        driver.getTitle();
        Assert.assertEquals(driver.getTitle(),"Contact us - My Store");
    }

    @AfterSuite(enabled = false)
    public void terminate()
    {
        driver.quit();
    }
}
