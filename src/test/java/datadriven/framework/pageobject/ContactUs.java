package datadriven.framework.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ContactUs {

    WebDriver driver;

    @FindBy(id="contact-link")
    private WebElement clickcontactbutton;

    @FindBy(id="id_contact")
    private WebElement subjectheading;

    @FindBy(id="email")
    private WebElement email;

    @FindBy(id="id_order")
    private WebElement orderreference;

    @FindBy(id="fileUpload")
    private WebElement fileupload;

    @FindBy(id="message")
    private  WebElement message;

    @FindBy(id="submitMessage")
    private WebElement send;

    public  ContactUs(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public ContactUs keyContactClick()
    {
        this.clickcontactbutton.click();
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.titleContains("Contact us - My Store"));
        return this;
    }
    public ContactUs keyClickSubjectHeading(String i)
    {
        Select select = new Select(this.subjectheading);
        select.selectByValue(i);
        return this;
    }
    public ContactUs keyEmail(String email)
    {
        this.email.sendKeys(email);
        return this;

    }
    public ContactUs keyOrderReference(String order)
    {
        this.orderreference.sendKeys(order);
        return this;
    }
    public ContactUs keyFileUpload()
    {
        Path path = Paths.get("src/test/java/resources/hello.txt");

        this.fileupload.sendKeys("path");
        return this;
    }
    public ContactUs keyMessage(String message)
    {
        this.message.sendKeys(message);
        return  this;
    }
    public WebElement keySend()
    {
        this.send.click();
        return null;
    }

}