package datadriven.framework.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Index {

WebDriver driver;

@FindBy(xpath = "//div[@class='header_user_info']/a")
private WebElement signIn;

@FindBy(xpath="//div[@id='contact-link']/a")
private WebElement contactUs;

@FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[3]/a")
private WebElement tshirt;

public Index(WebDriver driver){

    this.driver=driver;
    PageFactory.initElements(driver,this);

}

public SignInPage clickSignIn(){

this.signIn.click();
return new SignInPage(this.driver);

}

public ContactUs clickContactUs(){

    this.contactUs.click();
    return new ContactUs(this.driver);
}

public TShirts clickTShirt(){

    this.tshirt.click();
    return new TShirts(this.driver);
}




}
